FROM apache/airflow:2.5.2

USER root 
WORKDIR /usr/src
RUN apt-get update
RUN apt install wget
RUN apt install unzip  
RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
RUN apt -y install ./google-chrome-stable_current_amd64.deb
RUN wget  https://chromedriver.storage.googleapis.com/114.0.5735.90/chromedriver_linux64.zip


RUN unzip chromedriver_linux64.zip
USER airflow


WORKDIR /opt/airflow

RUN pip install --upgrade pip 

COPY requirements.txt ./

RUN pip install -r requirements.txt


# docker build . -t "airflow_server"
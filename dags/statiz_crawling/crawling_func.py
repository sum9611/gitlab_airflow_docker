# 크롤링 데이터 전처리 함수 
def transform_pitching_data(pitching_list,team,now):
    player_list = []
    for i in pitching_list:
        date = now
        name = i.split('>')[1].split('<')[0]
        birth = i.split('"')[0]
        today_type = i.split('(')[1].split(',')[0]
        if '"popup/pitlog.php' in today_type:
            today_type = '' 
        IP = i.split('<b>')[1].split('</b>')[0]
        TBF = i.split('<td>')[1].split('</td>')[0]
        H = i.split('<td>')[2].split('</td>')[0]
        R = i.split('<td>')[3].split('</td>')[0]
        ER = i.split('<td>')[4].split('</td>')[0]
        BB = i.split('<td>')[5].split('</td>')[0]
        HBP = i.split('<td>')[6].split('</td>')[0]
        K = i.split('<td>')[7].split('</td>')[0]
        HR = i.split('<td>')[8].split('</td>')[0]
        GO_FO = i.split('<td align="center">')[2].split('</td>')[0]
        PIT_S = i.split('<td align="center">')[3].split('</td>')[0]
        IR_IS = i.split('<td align="center">')[4].split('</td>')[0]
        GSC = i.split('<td>')[9].split('</td>')[0]
        ERA = i.split('<td align="center">')[5].split('</td>')[0]
        WHIP = i.split('<td align="center">')[6].split('</td>')[0]
        LI = i.split('<td>')[10].split('</td>')[0]
        WPA = i.split('<td>')[11].split('</td>')[0]
        RE24 = i.split('<td>')[12].split('</td>')[0]
        player_list.append([date, name,birth,team,today_type,IP,TBF,H,R,ER,BB,HBP,K,HR,GO_FO,PIT_S,IR_IS,GSC,ERA,WHIP,LI,WPA,RE24])
    return player_list

def transform_batting_data(batting_list, team,now):
    player_list = []
    for i in batting_list:
        date = now
        name = i.split('>')[1].split('<')[0]
        birth = i.split('"')[0]
        P = i.split('<td>')[1].split('</td>')[0]
        TPA = i.split('<b>')[1].split('</b>')[0]
        AB = i.split('<td>')[3].split('</td>')[0]
        R = i.split('<td>')[4].split('</td>')[0]
        H = i.split('<td>')[5].split('</td>')[0]
        HR = i.split('<td>')[6].split('</td>')[0]
        RBI = i.split('<td>')[7].split('</td>')[0]
        BB = i.split('<td>')[8].split('</td>')[0]
        HBP = i.split('<td>')[9].split('</td>')[0]   
        SO = i.split('<td>')[10].split('</td>')[0]
        GO = i.split('<td>')[11].split('</td>')[0]
        FO = i.split('<td>')[12].split('</td>')[0]
        PIT = i.split('<td>')[13].split('</td>')[0]
        GDP = i.split('<td>')[14].split('</td>')[0]
        LOB = i.split('<td>')[15].split('</td>')[0]
        AVG = i.split('<td>')[16].split('</td>')[0]
        OPS = i.split('<td>')[17].split('</td>')[0]
        LI = i.split('<td>')[18].split('</td>')[0]
        WPA = i.split('<td>')[19].split('</td>')[0]
        RE24 = i.split('<td>')[20].split('</td>')[0]
        
        player_list.append([date, name,birth,team,P,TPA,AB,R,H,HR,RBI,BB,HBP,SO,GO,FO,PIT,GDP,LOB,AVG,OPS,LI,WPA,RE24])
    return player_list


def data_split(batting_list,pitching_list,y_date):
    
    #선수하나하나의 기록 저장용
    temp_list = []

    # 전체 선수의 기록 저장
    temp_list2 = []

    # 타석 수 저장
    tpa_list = []
    
    # 구단명 저장
    team_list = []
    
    for hometeam in batting_list:
        team = hometeam.split(' ')[0]
        team_list.append(team)
        for player_list in hometeam.split('\n번 타자\n')[1:]:
            #모든 선수 list에 넣기
            for i in player_list.split('교체\n'):
                for j in i.split('\n\n')[0].replace('\n', '\t').split('\t'):
                    if j != '':
                        temp_list.append(j)
                tpa_list.append([len(temp_list[10:]),team])
                temp_list2.append(temp_list[0:10])
                temp_list = []
                cnt = 0
                
    batting = pd.DataFrame(temp_list2, columns= ['player_name','P','AB','R','H','RBI','HR','BB','SO','AVG'])
    tpa = pd.DataFrame(tpa_list,columns= ['TPA','TEAM'])
    batting_result = pd.concat([batting,tpa], axis=1)
    temp_list2 = []
    for hometeam,team in zip(pitching_list,team_list):
        
        #개인별 정보 전처리 for문
        for player_list in hometeam.split('내림차순 정렬\n\n\n')[-1].split('합계')[0].replace('\t','\n').replace(' ⅔', '.2').replace(' ⅓','.1').split('\n\n')[:-1]:
            temp_list = player_list.split()[:-5]
            if temp_list[1] == '홀' or temp_list[1] == '패' or temp_list[1] == '승' or temp_list[1] == '세':
                t_type = temp_list.pop(1)
                temp_list.append(t_type)
            else:
                temp_list.append('')
            temp_list.append(team)
            temp_list2.append(temp_list)
    pitching_result = pd.DataFrame(temp_list2,columns=['player_name','IP','H','R','ER','BB','K','HR','TBF','AB','PIT','Record','TEAM'])
    
    pitching_result['yyyymmdd'] = int(y_date)
    batting_result['yyyymmdd'] = int(y_date)
    
    
    
    
    return batting_result,pitching_result


def id_split(info, team, types):
    id_list = []
    if types == 'batting':
        for i,j in zip(info,team):
            for ID in i.split('playerId=')[1:]:
                id_list.append([ID.split('"')[0], ID.split('class="PlayerRecord_name__1W_c0">')[1].split('</span><span class="PlayerRecord_position__3SBbd">')[0],j])

    elif types == 'pitching':
        for i,j in zip(info,team):
            for ID in i.split('playerId=')[1:]:
                id_list.append([ID.split('"')[0], ID.split('class="PlayerRecord_name__1W_c0">')[1].split('</span>')[0],j])
    result = pd.DataFrame(id_list,columns=['player_ID','player_name','TEAM'])
    return result
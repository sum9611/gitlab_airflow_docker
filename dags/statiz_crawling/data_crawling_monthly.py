# 필요 모듈 import
from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.python import PythonOperator
import pymysql
import yaml
from bs4 import BeautifulSoup
import pandas as pd
from selenium import webdriver
import re
from selenium.webdriver.common.by import By
from sqlalchemy import create_engine
import os 
import pendulum
import urllib.request
from statiz_crawling.crawling_func import  transform_pitching_data, transform_batting_data
from pytz import timezone

local_tz = pendulum.timezone("Asia/Seoul")


with open('yamls/sql_info.yaml') as f:

    info = yaml.load(f, Loader=yaml.FullLoader)

# DAG 정의
dag = DAG(
    'data_crawling_monthly',        
    default_args={                  
        'retries': 1,               
        },
    schedule_interval='12 12 1 * *',      
    start_date=datetime(2023,4, 3, tzinfo = local_tz),
    catchup=False  
)

# 경기날짜 변수 선언
# 배치 시작일 기준 어제 날짜 설정 -- > x
## 수동 실행을 위해 어제날짜가 아닌 해당 월의 1일로 설정 
yesterday = datetime.now(timezone('Asia/Seoul')).replace(day=1) - timedelta(1)
days = int(yesterday.strftime("%d"))
month = int(yesterday.strftime("%m"))



# task 함수 설정
def crawling_monthly():
    for i in range(1,days+1):
        now = yesterday.strftime("%Y-%m-") + str(i).zfill(2)
        print(' ')
        print(now, '경기 크롤링!!')
        print(' ')
        # 크롤링 사이트
        target_url = f'http://www.statiz.co.kr/schedule.php?opt={month}&sy=2023'
        html = urllib.request.urlopen(target_url).read()
        bsObject = BeautifulSoup(html, 'html.parser')

        init_url = str(bsObject.find_all('table', {'class' : 'table table-striped table-bordered'})).split(f'boxscore.php?date={now}&amp;')[-5:]
        batting = pd.DataFrame()
        pitching = pd.DataFrame()  
        hometeam_list = ['-']

        # 경기마다 하나씩 출력 
        for i in init_url:
                
            # 크롤링 가능 주소인지 확인하는 로직
            if i[:8] == 'stadium=':
                print('=' * 50)
                stadium = str(i).split('>')[0]
                url = f'http://www.statiz.co.kr/boxscore.php?opt=4&date={now}&{stadium}'
                html = urllib.request.urlopen(url).read()
                bsObject = BeautifulSoup(html, 'html.parser')
                
                # 홈팀
                home_batting = str(bsObject.find_all('h3')[1])
                hometeam = home_batting.split('(')[1].split(')')[0]
                
                # 원정팀 
                away_batting = str(bsObject.find_all('h3')[2])
                awayteam = away_batting.split('(')[1].split(')')[0]
                
                # 데이터가 크롤링된 경우 제외히는 로직
                if hometeam in hometeam_list:
                    print(f'{hometeam} vs {awayteam} 경기는 이미 크롤링되었습니다.')
                    
                else:
                    hometeam_list.append(hometeam)
                    print(f'{hometeam} vs {awayteam} 경기 데이터 크롤링 !')
                    
                    # 타자 컬럼 데이터 추출
                    batting_columns = ['yyyymmdd', 'player_name', 'player_birth', 'team']
                    for i in bsObject.find_all('th')[2:22]:
                        batting_columns.append(str(i).split('<th>')[1].split('</th>')[0])
                        
                    ############################
                    ## 홈팀 데이터 추출 진행 ! ##  
                    ############################

                    # 홈팀 타자들의 정보 리스트로 담아두기
                    home_batting_list = []
                    for i in str(bsObject.find_all('table')[3]).split('birth=')[1:]:
                        home_batting_list.append(i)

                    # 타자별 정보 분리 및 2차월 배열로 저장           
                    home_player_list = transform_batting_data(home_batting_list, hometeam,now)
                    home_team_batting = pd.DataFrame(home_player_list,columns=batting_columns)            

                    ##############################
                    ## 원정팀 데이터 추출 진행 ! ##  
                    ##############################
                    
                    # 원정팀 타자들의 정보 리스트로 담아두기
                    away_batting_list = []
                    for i in str(bsObject.find_all('table')[5]).split('birth=')[1:]:
                        away_batting_list.append(i)
                    
                    # 타자별 정보 분리 및 2차월 배열로 저장
                    away_player_list = transform_batting_data(away_batting_list, awayteam,now)
                    away_team_batting = pd.DataFrame(away_player_list,columns=batting_columns)
                    

                    # 홈팀, 원정팀 정보 concat
                    batting = pd.concat([batting,home_team_batting,away_team_batting])
                    
                    
                    ##############################
                    ## 타자 정보가져오기 완료 !! ##
                    ##############################

                    # 타자 컬럼 데이터 추출
                    pitching_columns = ['yyyymmdd', 'player_name', 'player_birth', 'team','today_type']
                    for i in str(bsObject.find_all('table')[7]).split('<th>')[2:]:
                        
                        # 주석처리된 지표 핸들링
                        if i.split('</th>')[0] != 'RS':
                            pitching_columns.append(i.split('</th>')[0])

                    ############################
                    ## 홈팀 데이터 추출 진행 ! ##  
                    ############################

                    # 홈팀 투수들의 정보 리스트로 담아두기
                    home_pitching_list = []
                    for i in str(bsObject.find_all('table')[7]).split('birth=')[1:]:
                        home_pitching_list.append(i)

                    # 투수별 정보 분리 및 2차월 배열로 저장
                    player_list = transform_pitching_data(home_pitching_list, hometeam,now)
                    home_team_pitching = pd.DataFrame(player_list,columns=pitching_columns)

                    ##############################
                    ## 원정팀 데이터 추출 진행 ! ##  
                    ##############################

                    # 원정팀 투수들의 정보 리스트로 담아두기
                    away_pitching_list = []
                    for i in str(bsObject.find_all('table')[9]).split('birth=')[1:]:
                        away_pitching_list.append(i)

                    # 투수별 정보 분리 및 2차월 배열로 저장
                    player_list = transform_pitching_data(away_pitching_list, awayteam,now)
                    away_team_pitching = pd.DataFrame(player_list,columns=pitching_columns)

                    # 홈팀, 원정팀 정보 concat
                    pitching = pd.concat([pitching,home_team_pitching,away_team_pitching])
            else:
                print('유효하지않은 url입니다.')
        
        if len(pitching) == 0 and len(batting) == 0:
            print(f'{now} 모든 경기 데이터가 존재하지 않습니다!')
        else:
            # 데이터 적재 전 전처리         
            batting['yyyymmdd'] = batting['yyyymmdd'].apply(lambda x : x[0:4] + x[5:7] + x[8:10])
            pitching['yyyymmdd'] = pitching['yyyymmdd'].apply(lambda x : x[0:4] + x[5:7] + x[8:10])

            # [타자] 전처리
            batting['LI'] = batting['LI'].apply(lambda x : '0' if x =='' else x)
            batting['LI'] = batting['LI'].astype(float)        

            # [투수] 전처리
            pitching['LI'] = pitching['LI'].apply(lambda x : '0' if x =='' else x)
            pitching['LI'] = pitching['LI'].astype(float)
            pitching['GSC'] = pitching['GSC'].apply(lambda x : '0' if x =='' else x)
            pitching['GSC'] = pitching['GSC'].astype(float)
            pitching['GSC'] = pitching['GSC'].apply(lambda x : '0' if x =='' else x)
            pitching['GSC'] = pitching['GSC'].astype(float)
            pitching['ERA'] = pitching['ERA'].astype(float)



            # db 정보 가져오기         
            host = info['MARIADB']['IP']
            user = info['MARIADB']['USER']
            passwd=info['MARIADB']['PASSWD']
            db = info['MARIADB']['DB']
            port = info['MARIADB']['PORT']


            ##############################
            ######### 데이터 적재 #########
            ##############################

            ## 기존 데이터 trucate 후 적재 
            date = now[0:4] + now[5:7] + now[8:10]

            conn = pymysql.connect(host = host, user = user, passwd=passwd, db = db, charset='utf8', port = port,cursorclass=pymysql.cursors.DictCursor)
            cur = conn.cursor()
            sql = f'''ALTER TABLE batting_info TRUNCATE PARTITION  p{date}
            '''
            cur.execute(sql)

            sql = f'''ALTER TABLE pitching_info TRUNCATE PARTITION  p{date}
            '''
            cur.execute(sql)

            conn.close()


            # # [타자] to_sql로 밀어넣기 
            engine = create_engine(f"mysql+pymysql://{user}:{passwd}@{host}:{port}/{db}?charset=utf8")
            conn = engine.connect()
            batting.to_sql(name = 'batting_info', con = engine, if_exists = 'append', index=False)
            conn.close()

            # [투수] to_sql로 밀어넣기
            engine = create_engine(f"mysql+pymysql://{user}:{passwd}@{host}:{port}/{db}?charset=utf8")
            conn = engine.connect()
            pitching.to_sql(name = 'pitching_info', con = engine, if_exists = 'append', index=False)
            conn.close()


            print(pitching)
            
            print('')
            
            print(batting)

    
    


    
# task 설정
t1 = PythonOperator( 
    task_id = 'crawling_monthly', #task 이름 설정
    python_callable=crawling_monthly, # 불러올 함수 설정
    dag=dag #dag 정보 
)


# task 진행
t1
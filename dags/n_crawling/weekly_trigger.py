from airflow import DAG
from datetime import datetime, timedelta
from pytz import timezone
from airflow.operators.trigger_dagrun import TriggerDagRunOperator
from airflow.operators.python import BranchPythonOperator

# DAG 정의
dag = DAG(
    'weekly_trigger',        
    default_args={                  
        'retries': 1,               
        },
    schedule_interval='0 21 * * 1',       
    start_date=datetime(2023,4, 3),
    catchup=False  
)



trigger_weekly = TriggerDagRunOperator(
        task_id="trigger_weekly",
        trigger_dag_id="n_data_agg_weekly",
        wait_for_completion=True,
        dag = dag
    )
trigger_rank = TriggerDagRunOperator(
        task_id="trigger_rank",
        trigger_dag_id="n_data_agg_rank",
        wait_for_completion=True,
        dag = dag
    )


trigger_weekly >> trigger_rank
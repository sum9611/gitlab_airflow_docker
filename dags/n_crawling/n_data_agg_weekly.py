# # 필요 모듈 import
from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.python import PythonOperator
import pymysql
import yaml
import pandas as pd
from sqlalchemy import create_engine
from pytz import timezone

with open('yamls/sql_info.yaml') as f:

    info = yaml.load(f, Loader=yaml.FullLoader)

host = info['MARIADB']['IP']
user = info['MARIADB']['USER']
passwd=info['MARIADB']['PASSWD']
db = info['MARIADB']['DB']
port = info['MARIADB']['PORT']

# DAG 정의
dag = DAG(
    'n_data_agg_weekly',        
    default_args={                  
        'retries': 1,               
        },
    # schedule_interval='0 21 * * 1',   
    schedule_interval='@once',         
    start_date=datetime(2023,4, 3),
    catchup=False  
)

def data_agg():
    # 추출 기간 설정 (지난주 월요일 ~ 금주 월요일)
    today = datetime.now(timezone('Asia/Seoul')) 
    s_date = (today - timedelta(7)).strftime("%Y%m%d")
    e_date = (today - timedelta(1)).strftime("%Y%m%d")
    # s_date = '20230626'
    # e_date = '20230702'
    week = (today - timedelta(7)).isocalendar()[1]
    print(f'{week}주차 ({s_date} ~ {e_date}) 데이터 집계 시작')

    # 주차 데이터 select 
    conn = pymysql.connect(host = host, user = user, passwd=passwd, db = db, charset='utf8', port = port,cursorclass=pymysql.cursors.DictCursor)
    cur = conn.cursor()
    sql = f'''select * from batting_info
    where 1=1
    and yyyymmdd between {s_date} and {e_date}
    '''
    cur.execute(sql)

    result = cur.fetchall()
    batting = pd.DataFrame(result)

    sql = f'''select * from pitching_info
    where 1=1
    and yyyymmdd between {s_date} and {e_date}
    '''
    cur.execute(sql)

    result = cur.fetchall()
    pitching = pd.DataFrame(result)

    conn.close()

    # isocalendar 모듈을 사용하여 week 계산
    batting['week'] = batting['yyyymmdd'].astype(str).apply(lambda x :  x[0:4] + str(datetime.strptime(x[0:4] + '-'+ x[4:6]+'-'+x[6:8] ,'%Y-%m-%d').isocalendar()[1]))
    pitching['week'] = pitching['yyyymmdd'].astype(str).apply(lambda x :  x[0:4] + str(datetime.strptime(x[0:4] + '-'+ x[4:6]+'-'+x[6:8] ,'%Y-%m-%d').isocalendar()[1]))

    # 투수 이닝수 전처리하기 
    pitching['IP2'] = pitching['IP'].astype(str).apply(lambda x : int(x.split('.')[1]))
    pitching['IP'] = pitching['IP'].astype(str).apply(lambda x : int(x.split('.')[0]))
    

    # # 주별 합산 지표 컬럼만 추리기
    batting_week = batting.groupby(['week','player_name','player_ID','TEAM']).sum(numeric_only=False)[['TPA','AB','R','H', 'HR','RBI','BB','SO']].reset_index()
    pitching_week = pitching.groupby(['week','player_name','player_ID','TEAM']).sum(numeric_only=False)[['Record','IP','IP2','TBF','H','R','ER','BB','K','HR','AB','PIT']].reset_index()

    # 규정 타석, 이닝을 계산하기위한 주별 팀 경기수 변수 생성
    team_games = batting.groupby(['week','TEAM','yyyymmdd']).count().reset_index()
    team_games = team_games.groupby(['week','TEAM']).count().reset_index()[['week','TEAM','yyyymmdd']]
    team_games['game_count'] = team_games['yyyymmdd']
    team_games = team_games[['week','TEAM','game_count']]


    batting_week = batting_week.merge(team_games, on = ['week', 'TEAM'], how = 'left')
    pitching_week = pitching_week.merge(team_games, on = ['week', 'TEAM'], how = 'left')


    batting_week['RTPA'] = batting_week['game_count']*3.1
    batting_week['AVG'] = batting_week['H']/batting_week['AB']
    batting_week['AVG'] =batting_week['AVG'].fillna(0.0) 
    batting_week = batting_week[['week', 'player_name', 'player_ID', 'TEAM', 'TPA','RTPA', 'AB', 'R', 'H',
        'HR', 'RBI', 'BB', 'SO','AVG','game_count']]


    pitching_week['RIP'] = pitching_week['game_count'] * 1.0
    
    # 투수 이닝 합산
    pitching_week['IP'] = pitching_week['IP'] + pitching_week['IP2'].apply(lambda x : x//3) + pitching_week['IP2'].apply(lambda x : x%3/10) 
    pitching_week = pitching_week[['week', 'player_name', 'player_ID', 'TEAM', 'Record', 'IP', 'RIP','TBF', 'H', 'R', 'ER', 'BB', 'K', 'HR','PIT', 'game_count']]
    
    print("batting_week 데이터 적재 len : ", len(batting_week) )

    # 데이터 적재 
    engine = create_engine(f"mysql+pymysql://{user}:{passwd}@{host}:{port}/{db}?charset=utf8")
    conn = engine.connect()
    batting_week.to_sql(name = 'weekly_batting_info', con = engine, if_exists = 'append', index=False)
    conn.close()

    print("pitching_week 데이터 적재 len : ", len(pitching_week) )

    engine = create_engine(f"mysql+pymysql://{user}:{passwd}@{host}:{port}/{db}?charset=utf8")
    conn = engine.connect()
    pitching_week.to_sql(name = 'weekly_pitching_info', con = engine, if_exists = 'append', index=False)
    conn.close()

    print(pitching_week)
    print(batting_week)

# task 설정
t1 = PythonOperator( 
    task_id = 'data_agg', #task 이름 설정
    python_callable=data_agg, # 불러올 함수 설정
    dag=dag #dag 정보 
)


# task 진행
t1
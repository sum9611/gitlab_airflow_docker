# # 필요 모듈 import
from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.python import PythonOperator
import pymysql
import yaml
import pandas as pd
from sqlalchemy import create_engine
from pytz import timezone
from selenium import webdriver
import urllib.request
import time
from bs4 import BeautifulSoup
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from n_crawling.n_crawling_func import data_split, id_split

with open('yamls/sql_info.yaml') as f:

    info = yaml.load(f, Loader=yaml.FullLoader)

host = info['MARIADB']['IP']
user = info['MARIADB']['USER']
passwd=info['MARIADB']['PASSWD']
db = info['MARIADB']['DB']
port = info['MARIADB']['PORT']

# DAG 정의
dag = DAG(
    'n_crawling_daily',        
    default_args={                  
        'retries': 1,               
        },
    # schedule_interval='45 20 * * *',      
    schedule_interval='@once',      
    start_date=datetime(2023,4, 3),
    catchup=False  
)

# 경기날짜 변수선언
yesterday = datetime.now() - timedelta(1)

y_date = yesterday.strftime("%Y%m%d")
y_month= yesterday.strftime("%m")
y_year = yesterday.strftime("%Y")

def crawling():
    service = Service(executable_path='/usr/src/chromedriver')
    options = webdriver.ChromeOptions()
    options.add_argument('--no-sandbox')
    options.add_argument('--headless')
    options.add_argument('--disable-dev-shm-usage')
    options.add_argument("--single-process")

    target_url = f'https://sports.news.naver.com/kbaseball/schedule/index?date={y_date}&month={y_month}&year={y_year}&teamCode='
    html = urllib.request.urlopen(target_url).read()
    bsObject = BeautifulSoup(html, 'html.parser')
    init_url = str(bsObject.find_all('span', {'class' : 'td_btn'})).split('a href=')[1:]
    
    # xpath 선언부
    batting_xpath = '//*[@id="content"]/div/div/section[2]/div[2]/div/div[5]/div[2]/div[1]/div[2]/div/div/table'
    batting_xpath2 = '//*[@id="content"]/div/div/section[2]/div[2]/div/div[4]/div[2]/div[1]/div[2]/div/div/table'

    pitching_xpath = '//*[@id="content"]/div/div/section[2]/div[2]/div/div[5]/div[2]/div[2]'
    pitching_xpath2 = '//*[@id="content"]/div/div/section[2]/div[2]/div/div[4]/div[2]/div[2]'

    batting_id_xpath = '//*[@id="content"]/div/div/section[2]/div[2]/div/div[5]/div[2]/div[1]/div[2]/div/div/table/tbody'
    batting_id_xpath2 = '//*[@id="content"]/div/div/section[2]/div[2]/div/div[4]/div[2]/div[1]/div[2]/div/div/table/tbody'


    pitching_id_xpath = '//*[@id="content"]/div/div/section[2]/div[2]/div/div[5]/div[2]/div[2]/div[2]/div/div/table/tbody'
    pitching_id_xpath2 = '//*[@id="content"]/div/div/section[2]/div[2]/div/div[4]/div[2]/div[2]/div[2]/div/div/table/tbody'

    button_xpath = '//*[@id="content"]/div/div/section[2]/div[2]/div/div[5]/div[1]/div/button[1]'
    button_xpath2 = '//*[@id="content"]/div/div/section[2]/div[2]/div/div[4]/div[1]/div/button[1]'
    
    
    home_batting_list = []
    home_pitching_list = []
    home_batting_player_list = []
    home_pitching_player_list = []

    away_batting_list = []
    away_pitching_list = []
    away_batting_player_list = []
    away_pitching_player_list = []

    heme_team_list = []
    away_team_list = []
    
    #드라이버 연결
    driver = webdriver.Chrome(service=service, options=options)
    
    print('='*50)
    print(f'{y_date}날짜 추출')
    print('='*50)
    
    for i in init_url:
    # 경기결과 선택하기
        if 'record' in i:
            url = 'https://m.sports.naver.com' + i.split(' onclick')[0][1:-1]
            
            # 어제 경기 선택하기 
            if y_date in url:
                print(url, ' 경기로 이동')

                # 웹사이트 이동
                driver.get(url)

                time.sleep(5)
                
                # 당일 경기 홈팀 타격 정보 리스트에 저장
                try:
                    element = driver.find_element(By.XPATH, batting_xpath)
                except:
                    element = driver.find_element(By.XPATH, batting_xpath2)
                    
                data = element.get_attribute('innerText')
                print(f'홈팀타격 데이터 추출완료')
                home_batting_list.append(data)
                
                #홈팀 구단명 담기
                heme_team_list.append(data.split(' ')[0])
                
                # 당일 경기 홈팀 투수 정보 리스트에 저장
                try:
                    element = driver.find_element(By.XPATH, pitching_xpath)
                except:
                    element = driver.find_element(By.XPATH, pitching_xpath2)
                    
                data = element.get_attribute('innerText')
                print(f'홈팀투수 데이터 추출완료')
                home_pitching_list.append(data)

                # 타자 ID값
                try:
                    element = driver.find_element(By.XPATH, batting_id_xpath)
                except:
                    element = driver.find_element(By.XPATH, batting_id_xpath2)
                    
                data = element.get_attribute('innerHTML')
                home_batting_player_list.append(data)

                # 투수 ID값
                try:
                    element = driver.find_element(By.XPATH, pitching_id_xpath)
                except:
                    element = driver.find_element(By.XPATH, pitching_id_xpath2)
        
                data = element.get_attribute('innerHTML')
                home_pitching_player_list.append(data)

                
                try:    
                    button = driver.find_element(By.XPATH, button_xpath)
                    driver.execute_script("arguments[0].click();", button)
                except:
                    button = driver.find_element(By.XPATH, button_xpath2)
                    driver.execute_script("arguments[0].click();", button)
                    
                    
                
                # 당일 경기 원정팀 타격 정보 리스트에 저장
                try:
                    element = driver.find_element(By.XPATH, batting_xpath)
                except:
                    element = driver.find_element(By.XPATH, batting_xpath2)
                
                data = element.get_attribute('innerText')
                print(f'원정팀타격 데이터 추출완료')
                away_batting_list.append(data)
                
                #원정팀 구단명 담기
                away_team_list.append(data.split(' ')[0])
                
                # 당일 경기 원정팀 투수 정보 리스트에 저장
                try:
                    element = driver.find_element(By.XPATH, pitching_xpath)
                except:
                    element = driver.find_element(By.XPATH, pitching_xpath2)
                    
                data = element.get_attribute('innerText')
                print(f'원정팀투수 데이터 추출완료')
                away_pitching_list.append(data)
                
                # 타자 ID값
                try:
                    element = driver.find_element(By.XPATH, batting_id_xpath)
                except:
                    element = driver.find_element(By.XPATH, batting_id_xpath2)
                    
                data = element.get_attribute('innerHTML')
                away_batting_player_list.append(data)

                # 투수 ID값
                try:
                    element = driver.find_element(By.XPATH, pitching_id_xpath)
                except:
                    element = driver.find_element(By.XPATH, pitching_id_xpath2)
                    
                data = element.get_attribute('innerHTML')
                away_pitching_player_list.append(data)
                
                
               
    df_home_batting, df_home_pitching = data_split(home_batting_list,home_pitching_list,y_date)
    df_away_batting, df_away_pitching = data_split(away_batting_list,away_pitching_list,y_date)

    df_home_batting_info = id_split(home_batting_player_list,heme_team_list, 'batting')
    df_home_pitching_info = id_split(home_pitching_player_list,heme_team_list, 'pitching')


    df_away_batting_info = id_split(away_batting_player_list,away_team_list, 'batting')
    df_away_pitching_info = id_split(away_pitching_player_list,away_team_list, 'pitching')


    df_batting_info = pd.concat([df_home_batting_info, df_away_batting_info])
    df_pitching_info = pd.concat([df_home_pitching_info, df_away_pitching_info])


    df_batting = pd.concat([df_home_batting,df_away_batting])
    df_pitching = pd.concat([df_home_pitching,df_away_pitching])

    for i,j in zip(df_batting['player_name'], df_batting_info['player_name']):
        if i != j :
            print('선수정보 잘못됨')
    for i,j in zip(df_pitching['player_name'], df_pitching_info['player_name']):
        if i != j :
            print('선수정보 잘못됨')

    df_batting = pd.concat([df_batting,df_batting_info['player_ID']],axis=1)
    df_pitching = pd.concat([df_pitching,df_pitching_info['player_ID']],axis=1)

    df_batting = df_batting[['yyyymmdd','player_name','player_ID','TEAM','P','TPA','AB','R','H','HR','RBI','BB','SO','AVG']]
    df_pitching = df_pitching[['yyyymmdd','player_name','player_ID','TEAM','Record','IP','H','R','ER','BB','K','HR','TBF','AB','PIT']]
    
    
    engine = create_engine(f"mysql+pymysql://{user}:{passwd}@{host}:{port}/{db}?charset=utf8")
    conn = engine.connect()
    df_batting.to_sql(name = 'batting_info', con = engine, if_exists = 'append', index=False)
    conn.close()
    print('타자정보 적재완료!')

    engine = create_engine(f"mysql+pymysql://{user}:{passwd}@{host}:{port}/{db}?charset=utf8")
    conn = engine.connect()
    df_pitching.to_sql(name = 'pitching_info', con = engine, if_exists = 'append', index=False)
    conn.close()
    print('투수정보 적재완료!')
                        
    driver.close()
    
    
    print('=' * 50)
    print(df_batting)
    print('=' * 50)
    print(df_pitching)

# task 설정
t1 = PythonOperator( 
    task_id = 'crawling', #task 이름 설정
    python_callable=crawling, # 불러올 함수 설정
    dag=dag #dag 정보 
)


# task 진행
t1
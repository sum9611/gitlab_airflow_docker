import pandas as pd
import pymysql
import yaml

with open('yamls/sql_info.yaml') as f:

    info = yaml.load(f, Loader=yaml.FullLoader)

host = info['MARIADB']['IP']
user = info['MARIADB']['USER']
passwd=info['MARIADB']['PASSWD']
db = info['MARIADB']['DB']
port = info['MARIADB']['PORT']

def get_data(query):
    conn = pymysql.connect(host = host, user = user, passwd=passwd, db = db, charset='utf8', port = port,cursorclass=pymysql.cursors.DictCursor)
    cur = conn.cursor()
    cur.execute(query)
    result = cur.fetchall()
    df_result = pd.DataFrame(result)
    conn.close()
    return df_result

def update_data(query):
    print(query)
    conn = pymysql.connect(host = host, user = user, passwd=passwd, db = db, charset='utf8', port = port,cursorclass=pymysql.cursors.DictCursor)
    cur = conn.cursor()
    cur.execute(query)
    conn.commit()
    conn.close()


def point_logic(batting,info):
    temp_list = []
    temp_avg = 1000
    cnt = 11
    for name,id,avg in zip(batting['player_name'],batting['player_ID'],batting[f'{info}']):
        if cnt == 0:
            break

        if temp_avg == avg :
            temp_list.append([name,id,cnt])
        else:
            cnt -= 1
            if len(temp_list) >= 10:
                break
            else:
                temp_avg = avg
                temp_list.append([name,id,cnt])   

    return temp_list

def update_insert(result_list,player_type):
    if player_type == 'batting':

        for i in result_list:

            sql = f'''select * from rank_info 
                where 1=1
                and player_name = '{i[0]}'
                and player_ID = {i[1]}'''
            df_data = get_data(sql)

            if df_data.empty:
                print('data insert!')
                sql = f'''INSERT INTO rank_info (player_name,player_ID,player_type,total_point)
                        VALUES ('{i[0]}',{i[1]},'H',{i[2]})'''
                update_data(sql)
            else:
                print('data update!')
                sql = f'''UPDATE rank_info
                    SET total_point= total_point + {i[2]}
                    WHERE player_name='{i[0]}' AND player_ID={i[1]}'''
                update_data(sql)
                
    elif player_type == 'pitching':
        for i in result_list:

            sql = f'''select * from rank_info 
                where 1=1
                and player_name = '{i[0]}'
                and player_ID = {i[1]}'''
            df_data = get_data(sql)

            if df_data.empty:
                print('data insert!')
                sql = f'''INSERT INTO rank_info (player_name,player_ID,player_type,total_point)
                        VALUES ('{i[0]}',{i[1]},'P',{i[2]})'''
                update_data(sql)
            else:
                print('data update!')
                sql = f'''UPDATE rank_info
                    SET total_point= total_point + {i[2]}
                    WHERE player_name='{i[0]}' AND player_ID={i[1]}'''
                update_data(sql)

def data_split(batting_list,pitching_list,y_date):
    
    #선수하나하나의 기록 저장용
    temp_list = []

    # 전체 선수의 기록 저장
    temp_list2 = []

    # 타석 수 저장
    tpa_list = []
    
    # 구단명 저장
    team_list = []
    cnt2 = 0
    for hometeam in batting_list:
        team = hometeam.split(' ')[0]
        team_list.append(team)
        for player_list in hometeam.split('\n번 타자\n')[1:]:
            #모든 선수 list에 넣기
            for i in player_list.split('교체\n'):
                for j in i.split('\n\n')[0].replace('\n', '\t').split('\t'):
                    if j != '':
                        temp_list.append(j)
                for k in temp_list[10:]:
                    if '/' in k:
                        cnt2 += 1
                tpa_list.append([len(temp_list[10:]) + cnt2,team])
                temp_list2.append(temp_list[0:10])
                temp_list = []
                cnt2 = 0
                
    batting = pd.DataFrame(temp_list2, columns= ['player_name','P','AB','R','H','RBI','HR','BB','SO','AVG'])
    tpa = pd.DataFrame(tpa_list,columns= ['TPA','TEAM'])
    batting_result = pd.concat([batting,tpa], axis=1)
    temp_list2 = []
    for hometeam,team in zip(pitching_list,team_list):
        
        #개인별 정보 전처리 for문
        for player_list in hometeam.split('내림차순 정렬\n\n\n')[-1].split('합계')[0].replace('\t','\n').replace(' ⅔', '.2').replace(' ⅓','.1').split('\n\n')[:-1]:
            temp_list = player_list.split()[:-5]
            if temp_list[1] == '홀' or temp_list[1] == '패' or temp_list[1] == '승' or temp_list[1] == '세':
                t_type = temp_list.pop(1)
                temp_list.append(t_type)
            else:
                temp_list.append('')
            temp_list.append(team)
            temp_list2.append(temp_list)
    pitching_result = pd.DataFrame(temp_list2,columns=['player_name','IP','H','R','ER','BB','K','HR','TBF','AB','PIT','Record','TEAM'])
    
    pitching_result['yyyymmdd'] = int(y_date)
    batting_result['yyyymmdd'] = int(y_date)
    
    
    
    
    return batting_result,pitching_result


def id_split(info, team, types):
    id_list = []
    if types == 'batting':
        for i,j in zip(info,team):
            for ID in i.split('playerId=')[1:]:
                id_list.append([ID.split('"')[0], ID.split('class="PlayerRecord_name__1W_c0">')[1].split('</span><span class="PlayerRecord_position__3SBbd">')[0],j])

    elif types == 'pitching':
        for i,j in zip(info,team):
            for ID in i.split('playerId=')[1:]:
                id_list.append([ID.split('"')[0], ID.split('class="PlayerRecord_name__1W_c0">')[1].split('</span>')[0],j])
    result = pd.DataFrame(id_list,columns=['player_ID','player_name','TEAM'])
    return result
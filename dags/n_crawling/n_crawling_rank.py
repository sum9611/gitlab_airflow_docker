# # 필요 모듈 import
from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.python import PythonOperator
import pymysql
import yaml
import pandas as pd
from sqlalchemy import create_engine
from pytz import timezone
from n_crawling.n_crawling_func import get_data, point_logic,update_insert

with open('yamls/sql_info.yaml') as f:

    info = yaml.load(f, Loader=yaml.FullLoader)

host = info['MARIADB']['IP']
user = info['MARIADB']['USER']
passwd=info['MARIADB']['PASSWD']
db = info['MARIADB']['DB']
port = info['MARIADB']['PORT']

# DAG 정의
dag = DAG(
    'n_data_agg_rank',        
    default_args={                  
        'retries': 1,               
        },
    # schedule_interval='10 21 * * 1',  
    schedule_interval='@once',          
    start_date=datetime(2023,4, 3),
    catchup=False  
)

def make_rank():
    today = datetime.now(timezone('Asia/Seoul')) 
    s_date = (today - timedelta(7)).strftime("%Y%m%d")
    e_date = (today - timedelta(1)).strftime("%Y%m%d")
    week = (today - timedelta(7)).isocalendar()[1]
    print(f'{week}주차 ({s_date} ~ {e_date}) 랭킹집계 시작')
    week = (today - timedelta(7)).strftime('%Y') + str(week).zfill(2)
    sql = f'''select player_name, player_ID, ((ER * 9) / IP ) as `ERA` from weekly_pitching_info wpi 
                        where 1=1
                        and IP  >= RIP 
                        and week = {week}
                        order by `ERA` 
        '''
    result = get_data(sql)
    
    print(result)

    result_list = point_logic(result,'ERA')
    print(week)
    print(result_list)
    print('')
    print('')
    print('')
    update_insert(result_list,'pitching')



    sql = f'''select * from weekly_batting_info
                where 1=1
                and TPA >= RTPA
                and week = '{week}'
                order by AVG desc
        '''
    batting = get_data(sql)
    
    result_list = point_logic(batting,'AVG')
    print(week)
    print(result_list)
    print('')
    print('')
    print('')
    update_insert(result_list,'batting')

def data_load():

    today = datetime.now(timezone('Asia/Seoul')) 
    week = (today - timedelta(7)).isocalendar()[1]
    print(f'{week}주차 랭킹 history 적재')
    week = (today - timedelta(7)).strftime('%Y') + str(week).zfill(2)

    query = '''select * from rank_info'''
    df_rank = get_data(query)
    df_rank['week'] = week
    df_rank = df_rank[['week','player_name','player_ID','player_type','total_point']]

    engine = create_engine(f"mysql+pymysql://{user}:{passwd}@{host}:{port}/{db}?charset=utf8")
    conn = engine.connect()
    df_rank.to_sql(name = 'rank_info_history', con = engine, if_exists = 'append', index=False)
    conn.close()
    
    print('rank history 테이블 적재 완료')

# task 설정
t1 = PythonOperator( 
    task_id = 'make_rank', #task 이름 설정
    python_callable=make_rank, # 불러올 함수 설정
    dag=dag #dag 정보 
)

t2 = PythonOperator( 
    task_id = 'data_load', #task 이름 설정
    python_callable=data_load, # 불러올 함수 설정
    dag=dag #dag 정보 
)


# task 진행
t1 >> t2
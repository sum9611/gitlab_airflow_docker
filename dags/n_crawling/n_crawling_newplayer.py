# # 필요 모듈 import
from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.python import PythonOperator
import pymysql
import yaml
import pandas as pd
from sqlalchemy import create_engine
from pytz import timezone
from selenium import webdriver
import urllib.request
import time
from bs4 import BeautifulSoup
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from n_crawling.n_crawling_func import data_split, id_split
from selenium import webdriver

with open('yamls/sql_info.yaml') as f:

    info = yaml.load(f, Loader=yaml.FullLoader)

host = info['MARIADB']['IP']
user = info['MARIADB']['USER']
passwd=info['MARIADB']['PASSWD']
db = info['MARIADB']['DB']
port = info['MARIADB']['PORT']

# DAG 정의
dag = DAG(
    'n_crawling_newplayer',        
    default_args={                  
        'retries': 1,               
        },
    # schedule_interval='0 21 * * *',      
    schedule_interval='@once',      
    start_date=datetime(2023,4, 3),
    catchup=False  
)



def find_newplayer():

    print("신규선수찾기 시작!")
    conn = pymysql.connect(host = host, user = user, passwd=passwd, db = db, charset='utf8', port = port,cursorclass=pymysql.cursors.DictCursor)
    cur = conn.cursor()
    sql = '''select B.player_name, B.player_ID from player_info A  right outer join (
            select player_ID , player_name  from batting_info bi
            where 1=1 
            group by player_ID , player_name
            union 
            select player_ID , player_name  from pitching_info pi2 
            where 1=1 
            group by player_ID , player_name)B on A.player_name  = B.player_name and A.player_ID  = B.player_ID
            where 1=1
            and A.player_name is null
            and A.player_ID is null
            ''' 
    cur.execute(sql)
    result = cur.fetchall()
    player = pd.DataFrame(result)

    if len(player) != 0 :
        print(f'총 {len(player)}명의 선수가 새롭게 등록되었습니다. ')
        for i,j in zip(player['player_name'], player['player_ID']):
            print('선수 이름 : ', i, '선수 ID : ', j)

        engine = create_engine(f"mysql+pymysql://{user}:{passwd}@{host}:{port}/{db}?charset=utf8")
        conn = engine.connect()
        player.to_sql(name = 'player_info', con = engine, if_exists = 'append', index=False)
        conn.close()
        
        print('신규 선수 생년월일 찾기 시작')    
        
        id_list = player['player_ID'].to_list()
        service = Service(executable_path='/usr/src/chromedriver')
        options = webdriver.ChromeOptions()
        options.add_argument('--no-sandbox')
        options.add_argument('--headless')
        options.add_argument('--disable-dev-shm-usage')
        options.add_argument("--single-process")
        driver = webdriver.Chrome(service=service, options=options)

        # 국내 선수전용
        birth_xpath = '//*[@id="content"]/div/div/div[1]/div/div/dl[1]/dd[2]/span'

        # 용병 전용 
        birth_xpath2 = '//*[@id="content"]/div/div/div[1]/div/div/dl[1]/dd[3]/span'
        name_xpath = '//*[@id="_title_area"]/h2'
        result_list = []
        for i in id_list:
            url = f'https://m.sports.naver.com/player/index?from=sports&playerId={i}&category=kbo&tab=profile'
            driver.get(url)
            time.sleep(2)
            
            try: 
                element = driver.find_element(By.XPATH, name_xpath)
                
                name = element.get_attribute('innerText')
                print(f'{name}선수의 생년월일을 추출합니다. ID = {i}')

                element = driver.find_element(By.XPATH, birth_xpath)

                data = element.get_attribute('innerText')
                if '년' not in data and '월' not in data and '일' not in data:
                    element = driver.find_element(By.XPATH, birth_xpath2)
                    data = element.get_attribute('innerText')
                data = data.split(',')[0]
            except:
                print(f'ID {i} 선수는 생년월일 정보가 없습니다.')
                data = '신인선수'
                
            print(data)
            print('')
            result_list.append([i,data])
        
        df_result = pd.DataFrame(result_list,columns=['player_ID','player_birth'])
        engine = create_engine(f"mysql+pymysql://{user}:{passwd}@{host}:{port}/{db}?charset=utf8")
        conn = engine.connect()
        df_result.to_sql(name = 'player_birth', con = engine, if_exists = 'append', index=False)
        conn.close()

        driver.close()
        
    else:
        print('신규선수가 없습니다!')
    

# task 설정
t1 = PythonOperator( 
    task_id = 'find_newplayer', #task 이름 설정
    python_callable=find_newplayer, # 불러올 함수 설정
    dag=dag #dag 정보 
)


# task 진행
t1
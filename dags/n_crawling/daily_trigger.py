from airflow import DAG
from datetime import datetime, timedelta
from pytz import timezone
from airflow.operators.trigger_dagrun import TriggerDagRunOperator
from airflow.operators.python import BranchPythonOperator

# DAG 정의
dag = DAG(
    'daily_trigger',        
    default_args={                  
        'retries': 1,               
        },
    schedule_interval='45 20 * * *',        
    start_date=datetime(2023,4, 3),
    catchup=False  
)



trigger_daily = TriggerDagRunOperator(
        task_id="trigger_daily",
        trigger_dag_id="n_crawling_daily",
        wait_for_completion=True,
        dag = dag
    )
trigger_newplayer = TriggerDagRunOperator(
        task_id="trigger_newplayer",
        trigger_dag_id="n_crawling_newplayer",
        wait_for_completion=True,
        dag = dag
    )


trigger_daily >> trigger_newplayer
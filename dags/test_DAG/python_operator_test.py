# 필요 모듈 import
from datetime import datetime
from airflow import DAG
from airflow.operators.python import PythonOperator
import pendulum

local_tz = pendulum.timezone("Asia/Seoul")


# DAG 정의
dag = DAG(
    'DAG_test',         # DAG_Name
    default_args={                  
        'retries': 1,               # 실패시 재시도 횟수
        },
    schedule_interval='34 18 * * *',      
    start_date=datetime(2023,4, 3, tzinfo = local_tz),  # 배치 시작날짜 설정
    catchup=False
)

# 함수 설정
def print_test(**kwargs):
    test = kwargs['dag_run']
    print(test)

def test2(**context):
    test1 = context
    print(test1)
    test =  context["execution_date"]

    print('DAG 작동 시간 : ', test)
    print('날짜 : ', str(test)[:11])
    print('시간 : ', str(test)[11:])
# task 설정
t1 = PythonOperator( 
    task_id = 'print_test', #task 이름 설정
    python_callable=print_test, # 불러올 함수 설정
    dag=dag #dag 정보 
)
t2 = PythonOperator( 
    task_id = 'test2', #task 이름 설정
    python_callable=test2, # 불러올 함수 설정
    dag=dag #dag 정보 
)

# task 진행
t1 >> t2 